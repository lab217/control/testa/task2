from src import archive
import math

def test_simpsons1(test_func_1):
    result = archive.simpson(test_func_1, 1, 0, 2 * math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_simpsons2(test_func_2):
    result = archive.simpson(test_func_2, 1, 0.5, 1.9, intervals=1000)
    assert abs(result - 1.94607) <= 0.1


def test_rectangle1(test_func_1):
    result = archive.rectangle(test_func_1, 1, 0, 2 * math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_rectangle2(test_func_2):
    result = archive.rectangle(test_func_2, 1, 0.5, 1.9, intervals=10000)
    assert abs(result - 1.94607) <= 0.1


def test_trapezoid1(test_func_1):
    result = archive.trapezoid(test_func_1, 1, 0, 2 * math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_trapezoid2(test_func_2):
    result = archive.trapezoid(test_func_2, 1, 0.5, 1.9, intervals=10000)
    assert abs(result - 1.94607) <= 0.1